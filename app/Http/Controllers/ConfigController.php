<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConfigController extends Controller
{
    //
    public function home(){
        return view('home');
    }
    public function register(){
        return view('register');
    }

    public function kirim(Request $request){
        
        $firstnama = $request->firstnama;
        $lastnama = $request->lastnama;
        return view('welcome', compact('firstnama','lastnama'));
    }
}
